import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather_app/weather_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Weather App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final String _texti = "Click here to fetch";
  dynamic _response = "Response";
  late bool serviceEnabled;

  Future<void> fetchWeather() async {
    serviceEnabled = await Geolocator.isLocationServiceEnabled();

    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        debugPrint('Location Permission Denied');
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    WeatherService ws = WeatherService();
    ws.getWeather().then((value) => setState(() {
          _changeReponse(value);
        }));
  }

  void _changeReponse(result) {
    setState(() {
      _response = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextButton(onPressed: () => fetchWeather(), child: Text(_texti)),
          _response == "Response"
              ? Text(_response)
              : _WeatherDataView(
                  response: _response.data,
                )
        ],
      )),
    );
  }
}

class _WeatherDataView extends StatelessWidget {
  final response;
  const _WeatherDataView({Key? key, required this.response}) : super(key: key);

  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }

  final TextStyle commonTextStyle = const TextStyle(fontSize: 16);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          "Longitude: " + response["coord"]["lon"].toString() + "°",
          style: commonTextStyle,
        ),
        Text(
          "Latitude: " + response["coord"]["lat"].toString() + "°",
          style: commonTextStyle,
        ),
        Text(
          "Weather: " +
              response["weather"][0]["main"].toString() +
              " " +
              getWeatherIcon(response["weather"][0]["id"]) +
              ",  " +
              response["weather"][0]["description"].toString(),
          style: commonTextStyle,
        ),
        Text(
          "Temperature: " + response["main"]["temp"].toString() + "°",
          style: commonTextStyle,
        ),
        Text(
          "Temperature feels like: " +
              response["main"]["feels_like"].toString() +
              "°",
          style: commonTextStyle,
        ),
        Text(
          "Temperature max: " + response["main"]["temp_min"].toString() + "°",
          style: commonTextStyle,
        ),
        Text(
          "Temperature min: " + response["main"]["temp_max"].toString() + "°",
          style: commonTextStyle,
        ),
        Text(
          "Pressure: " + response["main"]["pressure"].toString(),
          style: commonTextStyle,
        ),
        Text(
          "Humidity: " + response["main"]["humidity"].toString(),
          style: commonTextStyle,
        ),
        Text(
          "Visibility: " + response["visibility"].toString(),
          style: commonTextStyle,
        ),
        Text(
          "Wind Speed / Degree: " +
              response["wind"]["speed"].toString() +
              "ms/" +
              response["wind"]["deg"].toString() +
              "°",
          style: commonTextStyle,
        ),
        Text(
          "Cloudiness: " + response["clouds"]["all"].toString() + "%",
          style: commonTextStyle,
        ),
        Text(
          "Date: " +
              DateTime.fromMicrosecondsSinceEpoch(response["dt"] * 1000000,
                      isUtc: true)
                  .toString(),
          style: commonTextStyle,
        ),
        Text(
          "City: " + response["name"].toString(),
          style: commonTextStyle,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text("Summary: ",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            Text(getMessage(response["main"]["temp"].toInt()),
                style: const TextStyle(fontSize: 18))
          ],
        )
      ],
    );
  }
}
