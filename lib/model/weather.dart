import "dart:convert";
import "package:weather_app/model/clouds.dart";
import "package:weather_app/model/coords.dart";
import 'package:weather_app/model/main.dart';
import 'package:weather_app/model/sys.dart';
import 'package:weather_app/model/weatherelement.dart';
import 'package:weather_app/model/wind.dart';

class Weather {
  String base;
  Coord coord;
  List<WeatherElement> weather;
  Main main;
  int visibility;
  Wind wind;
  Clouds clouds;
  int dt;
  Sys sys;
  int timezone;
  int id;
  String name;
  int cod;

  Weather({
    required this.coord,
    required this.weather,
    required this.base,
    required this.main,
    required this.visibility,
    required this.wind,
    required this.clouds,
    required this.dt,
    required this.sys,
    required this.timezone,
    required this.id,
    required this.name,
    required this.cod,
  });

  factory Weather.fromJson(Map<String, dynamic> json) => Weather(
      coord: Coord.fromJson(json["coord"]),
      weather: List<WeatherElement>.from(
          json["weather"].map((x) => WeatherElement.fromJson(x))),
      base: json["base"],
      main: json["main"],
      visibility: json["visibility"],
      wind: json["wind"],
      clouds: json["clouds"],
      dt: json["dt"],
      sys: json["sys"],
      timezone: json["timezone"],
      id: json["id"],
      name: json["name"],
      cod: json["cod"]);

  Map<String, dynamic> toJson() => {
        "coord": coord.toJson(),
        "weather": List<dynamic>.from(weather.map((x) => x.toJson())),
        "base": base,
        "main": main.toJson(),
        "visibility": visibility,
        "wind": wind.toJson(),
        "clouds": clouds.toJson(),
        "dt": dt,
        "sys": sys.toJson(),
        "timezone": timezone,
        "id": id,
        "name": name,
        "cod": "cod"
      };
}

Weather weatherFromJson(String str) => Weather.fromJson(json.decode(str));

String weatherToJson(Weather data) => json.encode(data.toJson());
